//
//  HCData.h
//
//  Created by Sourabh Goyal on 4/27/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface HCData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *key;
@property (nonatomic, assign) double pos;
@property (nonatomic, strong) NSString *type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
