//
//  GSCheckBox.h
//  OneAppMD
//
//  Created by CarDekho.com on 9/29/16.
//  Copyright © 2016 Girnar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Theme/Theme-Swift.h>

@class SVGKFastImageView;

@interface GSCheckBox : UIView

@property (nonatomic) SVGKFastImageView *checkBox;
@property (nonatomic) UILabel *textLable;
@property (nonatomic) UIButton *tapButton;

@property (nonatomic) IBInspectable BOOL isON;

@property (nonatomic) IBInspectable NSString *text;
@property (nonatomic) GS_StyleSheet styleSheet;
@property (nonatomic) IBInspectable CGFloat fontSize;
@property (nonatomic) GS_FontStyle fontStyle;

@property (nonatomic, strong) id target;
@property (nonatomic, assign) SEL tapAction;

- (void)addTarget:(id)target action:(SEL)action;

@end
