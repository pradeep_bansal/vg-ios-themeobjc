//
//  OAAddTableViewCell.h
//  DemoGoogleAdd
//
//  Created by Vasim Akram on 7/11/17.
//  Copyright © 2017 GirnarSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;
@import Theme;
@class HCData;

@interface OAAddTableViewCell : UITableViewCell

@property(nonatomic, strong) IBOutlet NSLayoutConstraint *bannerViewHeight;
@property(nonatomic, weak) IBOutlet DFPBannerView *bannerView;
@property(nonatomic, assign) UITableView *tableView;
@property(nonatomic, strong) HCData *addData;
@property(nonatomic, weak) IBOutlet GSLable *lblAd;
@property(nonatomic, weak) IBOutlet GSLable *lblLoading;

@end
