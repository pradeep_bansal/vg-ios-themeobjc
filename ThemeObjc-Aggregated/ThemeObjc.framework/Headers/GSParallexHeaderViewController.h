//
//  GSParallexHeaderViewController.h
//  LandingDemo
//
//  Created by Vasim Akram on 3/24/17.
//  Copyright © 2017 GirnarSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MXXSegmentedPager.h"

@interface GSParallexHeaderViewController : UIViewController

@property (nonatomic, strong) IBOutlet MXXSegmentedPager  * segmentedPager;
@property (nonatomic, weak) IBOutlet UITableView  * tableView;

@property (nonatomic, strong) UIView *parallaxHeader;
@property (nonatomic, strong) NSLayoutConstraint *segmentBottom;

@end
