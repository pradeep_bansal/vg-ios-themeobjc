//
//  GSSegmentPager.h
//  Theme
//
//  Created by Vasim Akram on 11/9/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Theme/Theme-Swift.h>

@class GSSegmentPager;

@protocol GSSegmentPagerDelegate <NSObject>

- (void)segmentPager:(GSSegmentPager *)segmentPager didMoveFromIndex:(NSInteger )fromIndex toIndex:(NSInteger )toIndex;

@optional
- (void)segmentPager:(GSSegmentPager *)segmentPager parallaxHeaderDidScrollWithProgress:(CGFloat)progress;

@end

@protocol GSSegmentPagerDataSource <NSObject>

- (NSInteger)noOfSegmentInSegmentPager:(GSSegmentPager *)segmentPager;
- (NSString *)segmentPager:(GSSegmentPager *)segmentPager titleForSegmentAtIndex:(NSInteger )index;
- (UIViewController *)segmentPager:(GSSegmentPager *)segmentPager viewControllerForSegmentAtIndex:(NSInteger )index;

@end

typedef NS_ENUM(NSInteger, GSSegmentTitleWidthStyle) {
    GSSegmentTitleWidthStyleFixed,
    GSSegmentTitleWidthStyleDynamic,
};




@interface GSSegmentPager : UIView


@property (nonatomic, assign) id <GSSegmentPagerDelegate> delegate;
@property (nonatomic, assign) id <GSSegmentPagerDataSource> dataSource;

@property (nonatomic, assign) NSInteger currentSegmentIndex;
@property (nonatomic, assign) BOOL scrollEnabled;
@property (nonatomic, assign) GSSegmentTitleWidthStyle segmentTitleWidthStyle;
@property (nonatomic, assign) BOOL dropSegmentShadow;
@property (nonatomic, assign) CGFloat parallaxHeaderHeight;
@property (nonatomic, strong) UIColor *segmentedControlBackgroundColor;
@property (nonatomic) GS_StyleSheet activeStyleSheet;
@property (nonatomic) GS_StyleSheet deactiveStyleSheet;
@property (nonatomic, strong) UIView *parallaxHeaderView;
@property (nonatomic) UIEdgeInsets segmentedControlEdgeInsets;

-(void)selectSegmentAtIndex:(NSInteger )index withAnimation:(BOOL)animation;
-(void)setParallaxHeaderView:(UIView *)view;
-(void)reloadData;

@end
