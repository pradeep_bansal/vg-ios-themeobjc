//
//  Theme.h
//  Theme
//
//  Created by Vasim Akram on 8/29/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Theme.
FOUNDATION_EXPORT double ThemeVersionNumber;

//! Project version string for Theme.
FOUNDATION_EXPORT const unsigned char ThemeVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Theme/PublicHeader.h>
