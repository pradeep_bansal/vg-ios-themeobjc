//
//  AMRatingView.h
//  Amelio
//
//  Created by Vasim Akram on 12/25/17.
//  Copyright © 2017 Vasim Akram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSRatingView : UIView

@property (nonatomic, assign) IBInspectable CGFloat value;
@property (nonatomic, assign) IBInspectable BOOL userInteraction;
@property (nonatomic, assign) IBInspectable NSInteger minimumValue;
@property (nonatomic, assign) IBInspectable NSInteger maximumValue;
@property (nonatomic, assign) IBInspectable BOOL allowsHalfStars;

-(void)addTarget:(id)target withAction:(SEL)action;

@end
