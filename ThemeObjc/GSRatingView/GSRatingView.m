//
//  AMRatingView.m
//  Amelio
//
//  Created by Vasim Akram on 12/25/17.
//  Copyright © 2017 Vasim Akram. All rights reserved.
//

#import "GSRatingView.h"
#import <HCSStarRatingView/HCSStarRatingView.h>

@interface GSRatingView(){
    HCSStarRatingView *ratingView;
}
    
@end

@implementation GSRatingView
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     - (void)drawRect:(CGRect)rect {
     // Drawing code
     }
     */
    
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self loadXib];
    }
    return self;
}
    
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self loadXib];
    }
    return self;
}
    
-(void)layoutSubviews{
    [super layoutSubviews];
    ratingView.frame = self.bounds;
}
    
-(void)loadXib{
    self.backgroundColor = [UIColor clearColor];
    ratingView = [[HCSStarRatingView alloc] init];
    [self addSubview:ratingView];
    ratingView.minimumValue = 0;
    ratingView.maximumValue = 5;
    ratingView.allowsHalfStars = YES;
    ratingView.accurateHalfStars = YES;
    ratingView.tintColor = [UIColor colorWithRed:2/255.0f green:82/255.0f blue:150/255.0f alpha:1];
    ratingView.tintColor = ratingView.tintColor;
    ratingView.userInteractionEnabled = NO;
}


-(CGFloat)value{
    return ratingView.value;
}

-(void)setValue:(CGFloat)value{
    ratingView.value = value;
}

-(void)setMinimumValue:(NSInteger)minimumValue{
    ratingView.minimumValue = minimumValue;
}

-(void)setMaximumValue:(NSInteger)maximumValue{
    ratingView.maximumValue = maximumValue;
}


-(void)setUserInteraction:(BOOL)userInteraction{
    ratingView.userInteractionEnabled = userInteraction;
}

-(void)setAllowsHalfStars:(BOOL)allowsHalfStars{
    ratingView.allowsHalfStars = allowsHalfStars;
}

-(void)addTarget:(id)target withAction:(SEL)action{
    [ratingView addTarget:target action:action forControlEvents:UIControlEventValueChanged];
}
    
@end
