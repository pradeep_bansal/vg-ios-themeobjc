//
//  GSParallexHeaderViewController.m
//  LandingDemo
//
//  Created by Vasim Akram on 3/24/17.
//  Copyright © 2017 GirnarSoft. All rights reserved.
//

#import "GSParallexHeaderViewController.h"

@interface GSParallexHeaderViewController ()<MXSegmentedPagerDataSource, MXSegmentedPagerDelegate>{
    
}

@end



@implementation GSParallexHeaderViewController

@synthesize parallaxHeader = _parallaxHeader;
@synthesize segmentBottom;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.segmentedPager.backgroundColor = [UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1];
    [self.view addSubview:self.segmentedPager];
    [self addSegmentControlConstraints];
    
    // Parallax Header
    self.segmentedPager.parallaxHeader.mode = MXParallaxHeaderModeTopFill;
    self.segmentedPager.parallaxHeader.height = 280;
    self.segmentedPager.parallaxHeader.minimumHeight = 64;
    
    
    self.segmentedPager.delegate    = self;
    self.segmentedPager.dataSource  = self;
    self.segmentedPager.segmentedControl.selectionIndicatorColor = [UIColor clearColor];
    self.segmentedPager.segmentedControl.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (MXXSegmentedPager *)segmentedPager {
    if (!_segmentedPager) {
        
        // Set a segmented pager below the cover
        _segmentedPager = [[MXXSegmentedPager alloc] init];
        _segmentedPager.delegate    = self;
        _segmentedPager.dataSource  = self;
    }
    return _segmentedPager;
}

-(void)setParallaxHeader:(UIView *)parallaxHeader{
    _parallaxHeader = parallaxHeader;
    self.segmentedPager.parallaxHeader.view = parallaxHeader;
}

-(void)addSegmentControlConstraints{
    self.segmentedPager.translatesAutoresizingMaskIntoConstraints = false;

    NSLayoutConstraint *segmentLeft = [NSLayoutConstraint constraintWithItem:self.segmentedPager attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
    NSLayoutConstraint *segmentRight = [NSLayoutConstraint constraintWithItem:self.segmentedPager attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    
    NSLayoutConstraint *segmentTop = nil;
   
    if (@available(iOS 11.0, *)) {
        segmentTop = [NSLayoutConstraint constraintWithItem:self.segmentedPager attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeTop multiplier:1 constant:-4];
        segmentBottom = [NSLayoutConstraint constraintWithItem:self.segmentedPager attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view.safeAreaLayoutGuide attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    }
    else{
        segmentTop = [NSLayoutConstraint constraintWithItem:self.segmentedPager attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        segmentBottom = [NSLayoutConstraint constraintWithItem:self.segmentedPager attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    }
    
    [self.view addConstraints:@[segmentLeft,segmentRight,segmentTop,segmentBottom]];
}


#pragma mark <MXSegmentedPagerDelegate>

- (CGFloat)heightForSegmentedControlInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return 44;
}


#pragma mark <MXSegmentedPagerDataSource>

- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return 1;
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index {
    return @"";
}

- (UIView *)segmentedPager:(MXSegmentedPager *)segmentedPager viewForPageAtIndex:(NSInteger)index {
    return self.tableView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
