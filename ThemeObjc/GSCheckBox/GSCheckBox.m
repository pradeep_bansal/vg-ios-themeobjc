//
//  GSCheckBox.m
//  OneAppMD
//
//  Created by CarDekho.com on 9/29/16.
//  Copyright © 2016 Girnar. All rights reserved.
//

#import "GSCheckBox.h"
#import <SVGKit/SVGKFastImageView.h>

@implementation GSCheckBox

@synthesize isON = _isON;

@synthesize text = _text;
@synthesize styleSheet = _styleSheet;
@synthesize fontSize = _fontSize;
@synthesize fontStyle = _fontStyle;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self configureCheckBox];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self configureCheckBox];
    }
    return self;
}


-(BOOL)isON{
    return _isON;
}
-(void)setIsON:(BOOL)isON{
    _isON = isON;
    @try {
        NSBundle *bundle = [NSBundle bundleForClass:self.class];
        SVGKImage *image = [SVGKImage imageNamed:@"oa-uncheck-box" inBundle:bundle];
        if(isON == YES){
            image = [SVGKImage imageNamed:@"oa-check-box" inBundle:bundle];
            [image updateImageWithColor:[ThemeManager sharedInstance].themeColor];
        }
        [self.checkBox setImage:image];
    } @catch (NSException *exception) {
        NSLog(@"::::GSCheckBox::::Exception:::::%@",exception.name);
    }
}

-(NSString *)text{
    return _text;
}
-(void)setText:(NSString *)text{
    _text = text;
    self.textLable.text = text;
    self.textLable.adjustsFontSizeToFitWidth = YES;
}

-(GS_StyleSheet)styleSheet{
    return _styleSheet;
}
-(void)setStyleSheet:(GS_StyleSheet)styleSheet{
    _styleSheet = styleSheet;
    self.textLable.font = [GSStyleSheet fontWithStyleSheet:styleSheet];
}

-(CGFloat)fontSize{
    return _fontSize;
}
-(void)setFontSize:(CGFloat)fontSize{
    _fontSize = fontSize;
    self.textLable.font = [GSFont fontWithStyle:self.fontStyle andSize:fontSize];
}

-(GS_FontStyle)fontStyle{
    return _fontStyle;
}
-(void)setFontStyle:(GS_FontStyle)fontStyle{
    _fontStyle = fontStyle;
    self.textLable.font = [GSFont fontWithStyle:fontStyle andSize:self.fontSize];
}


-(void)layoutSubviews{
    CGRect frame = self.frame;
    CGFloat layoutMargin = 8.0;
    CGFloat checkBoxHeight = 3.0 * frame.size.height / 5.0;
    
    self.checkBox.frame = CGRectMake(layoutMargin, (frame.size.height - checkBoxHeight) / 2.0, checkBoxHeight, checkBoxHeight);
    self.textLable.frame = CGRectMake(checkBoxHeight + 2*layoutMargin, layoutMargin/2.0, frame.size.width - checkBoxHeight -2*layoutMargin, frame.size.height - layoutMargin);
    self.tapButton.frame = CGRectMake(0,0,frame.size.width, frame.size.height);
}

-(void)configureCheckBox{
    if (self.checkBox == nil){
        self.checkBox = [[SVGKFastImageView alloc] initWithSVGKImage:nil];
        [self setIsON:NO];
    }
    if (self.textLable == nil){
        self.textLable = [[UILabel alloc] init];
       // self.textLable.textColor = [ThemeManager sharedInstance].themeColor;
    }
    if (self.tapButton == nil){
        self.tapButton = [[UIButton alloc] init];
    }
    
    [self addSubview:self.checkBox];
    [self addSubview:self.textLable];
    [self addSubview:self.tapButton];
    
    self.styleSheet = GS_StyleSheetLink;
    
    [self.tapButton addTarget:self action:@selector(didCheckBoxChangeStateAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setNeedsLayout];
    
}

-(IBAction)didCheckBoxChangeStateAction:(id)sender{
    self.isON = !self.isON;
    if(self.target != nil && [self.target respondsToSelector:self.tapAction]){
        [self.target performSelector:self.tapAction withObject:self afterDelay:0.2];
    }
}

- (void)addTarget:(id)target action:(SEL)action{
    self.target = target;
    self.tapAction = action;
}

@end
