//
//  GSProgressBar.m
//  OneAppMD
//
//  Created by CarDekho.com on 9/30/16.
//  Copyright © 2016 Girnar. All rights reserved.
//

#import "GSProgressBar.h"
#import <Theme/Theme-Swift.h>

@implementation GSProgressBar

@synthesize minimumValue = _minimumValue;
@synthesize maximumValue = _maximumValue;
@synthesize currentValue = _currentValue;
@synthesize progressColor = _progressColor;
@synthesize backColor = _backColor;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self configureProgressBar];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self configureProgressBar];
    }
    return self;
}

-(CGFloat)minimumValue{
    return _minimumValue;
}
-(void)setMinimumValue:(CGFloat)minimumValue{
    _minimumValue = minimumValue;
    [self setNeedsLayout];
}

-(CGFloat)maximumValue{
    return _maximumValue;
}
-(void)setMaximumValue:(CGFloat)maximumValue{
    _maximumValue = maximumValue;
    [self setNeedsLayout];
}

-(CGFloat)currentValue{
    return _currentValue;
}
-(void)setCurrentValue:(CGFloat)currentValue{
    _currentValue = currentValue;
    [self setNeedsLayout];
}

-(UIColor *)progressColor{
    return _progressColor;
}
-(void)setProgressColor:(UIColor *)progressColor{
    _progressColor = progressColor;
    [self setNeedsLayout];
}

-(UIColor *)backColor{
    return _backColor;
}
-(void)setBackColor:(UIColor *)backColor{
    _backColor = backColor;
    [self setNeedsLayout];
}



-(void)layoutSubviews{
    CGRect frame = self.frame;
    CGFloat progressWidth =  frame.size.width * _currentValue / (_maximumValue - _minimumValue) ;
    if (isnan(progressWidth) == YES){
        progressWidth = 0;
    }
    _filledView.frame = CGRectMake(0, 0, progressWidth, frame.size.height);
    
    self.layer.cornerRadius = frame.size.height / 2.0;
    self.filledView.layer.cornerRadius = frame.size.height / 2.0;
    
    //CGFloat progressPercentage = progressWidth / frame.size.width;
//    self.filledView.backgroundColor = [_progressColor colorWithAlphaComponent:progressPercentage];
    
    self.filledView.backgroundColor =  _progressColor;
    self.backgroundColor = _backColor;
}

-(void)configureProgressBar{
    if (self.filledView == nil){
        self.filledView = [[UIView alloc] init];
    }
    
    self.minimumValue = 0;
    self.maximumValue = 100;
    self.currentValue = 33;
    self.progressColor = [ThemeManager sharedInstance].themeColor;
    self.backColor = [UIColor lightGrayColor];
    
    [self addSubview:self.filledView];
    
    [self setNeedsLayout];
}


@end
