//
//  GSProgressBar.h
//  OneAppMD
//
//  Created by CarDekho.com on 9/30/16.
//  Copyright © 2016 Girnar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSProgressBar : UIView

@property (nonatomic) UIView *filledView;

@property (nonatomic) IBInspectable CGFloat minimumValue;
@property (nonatomic) IBInspectable CGFloat maximumValue;
@property (nonatomic) IBInspectable CGFloat currentValue;

@property (nonatomic) IBInspectable UIColor *progressColor;
@property (nonatomic) IBInspectable UIColor *backColor;

@end
