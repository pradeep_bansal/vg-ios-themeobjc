//
//  GSSegmentPager.m
//  Theme
//
//  Created by Vasim Akram on 11/9/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

#import "GSSegmentPager.h"
#import "MXXSegmentedPager.h"
#import <Theme/Theme-Swift.h>


@interface GSSegmentPager()<MXSegmentedPagerDataSource, MXSegmentedPagerDelegate>{
    NSInteger lastSelectedSegmentIndex;
    BOOL isTabZero;
}
@property (nonatomic, strong) MXSegmentedPager *segmentedPager;

@end


@implementation GSSegmentPager

@synthesize segmentedPager = _segmentedPager;
@synthesize currentSegmentIndex;
@synthesize delegate = _delegate;
@synthesize dataSource = _dataSource;
@synthesize segmentTitleWidthStyle = _segmentTitleWidthStyle;
@synthesize dropSegmentShadow = _dropSegmentShadow;
@synthesize parallaxHeaderHeight = _parallaxHeaderHeight;
@synthesize segmentedControlBackgroundColor = _segmentedControlBackgroundColor;
@synthesize activeStyleSheet = _activeStyleSheet;
@synthesize deactiveStyleSheet = _deactiveStyleSheet;
@synthesize  parallaxHeaderView = _parallaxHeaderView;
@synthesize segmentedControlEdgeInsets = _segmentedControlEdgeInsets;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self configureSegmentPager];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self configureSegmentPager];
    }
    return self;
}

-(void)layoutSubviews{
    self.segmentedPager.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

-(MXSegmentedPager *)segmentedPager{
    if(_segmentedPager == nil){
        if(_parallaxHeaderView){
            _segmentedPager = [[MXXSegmentedPager alloc] init];
        }
        else{
            _segmentedPager = [[MXSegmentedPager alloc] init];
        }
    }
    return _segmentedPager;
}

-(void)setDelegate:(id<GSSegmentPagerDelegate>)delegate{
    _delegate = delegate;
    self.segmentedPager.delegate = self;
}

-(void)setDataSource:(id<GSSegmentPagerDataSource>)dataSource{
    _dataSource = dataSource;
    self.segmentedPager.dataSource = self;
    if([dataSource isKindOfClass:[UIViewController class]] == YES){
        UIViewController *objViewController = (UIViewController *)dataSource;
        objViewController.automaticallyAdjustsScrollViewInsets = NO;
    }
}

-(void)setSegmentTitleWidthStyle:(GSSegmentTitleWidthStyle)segmentTitleWidthStyle{
    _segmentTitleWidthStyle = segmentTitleWidthStyle;
    self.segmentedPager.segmentedControl.segmentWidthStyle = (HMSegmentedControlSegmentWidthStyle)segmentTitleWidthStyle;
}

-(void)setDropSegmentShadow:(BOOL)dropSegmentShadow{
    _dropSegmentShadow = dropSegmentShadow;
    _segmentedPager.segmentedControl.layer.shadowColor = dropSegmentShadow ?[[UIColor blackColor] CGColor] : [[UIColor clearColor] CGColor] ;
    [_segmentedPager.segmentedControl setNeedsDisplay];
}

-(void)setParallaxHeaderHeight:(CGFloat)parallaxHeaderHeight{
    _parallaxHeaderHeight = parallaxHeaderHeight;
    _segmentedPager.parallaxHeader.height = parallaxHeaderHeight;
    [_segmentedPager setNeedsDisplay];
}

-(void)setSegmentedControlBackgroundColor:(UIColor *)segmentedControlBackgroundColor{
    _segmentedControlBackgroundColor = segmentedControlBackgroundColor;
    _segmentedPager.segmentedControl.backgroundColor = segmentedControlBackgroundColor;
    [_segmentedPager.segmentedControl setNeedsDisplay];
}

- (void)setSegmentedControlEdgeInsets:(UIEdgeInsets)segmentedControlEdgeInsets {
    _segmentedControlEdgeInsets = segmentedControlEdgeInsets;
    _segmentedPager.segmentedControlEdgeInsets = segmentedControlEdgeInsets;
    [_segmentedPager setNeedsLayout];
}


-(GS_StyleSheet)activeStyleSheet{
    return _activeStyleSheet;
}
-(void)setActiveStyleSheet:(GS_StyleSheet)activeStyleSheet{
    _activeStyleSheet = activeStyleSheet;
    _segmentedPager.segmentedControl.selectedTitleTextAttributes = _segmentedPager.parallaxHeader.view == nil ?
    @{
      NSForegroundColorAttributeName : [GSStyleSheet colorWithStyleSheet:activeStyleSheet],
      NSFontAttributeName : [GSStyleSheet fontWithStyleSheet:activeStyleSheet]
      }
    :
    @{
      NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:1.0],
      NSFontAttributeName : [GSStyleSheet fontWithStyleSheet:activeStyleSheet]
      }
    ;
}

-(GS_StyleSheet)deactiveStyleSheet{
    return _deactiveStyleSheet;
}
-(void)setDeactiveStyleSheet:(GS_StyleSheet)deactiveStyleSheet{
    _deactiveStyleSheet = deactiveStyleSheet;
    _segmentedPager.segmentedControl.titleTextAttributes = _segmentedPager.parallaxHeader.view == nil ?
    @{
      NSForegroundColorAttributeName : [GSStyleSheet colorWithStyleSheet:deactiveStyleSheet],
      NSFontAttributeName : [GSStyleSheet fontWithStyleSheet:deactiveStyleSheet]
      }
    :
    @{
      NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:0.69],
      NSFontAttributeName : [GSStyleSheet fontWithStyleSheet:deactiveStyleSheet]
      }
    ;
}

-(NSInteger)currentSegmentIndex{
    return _segmentedPager.segmentedControl.selectedSegmentIndex;
}

-(void)setScrollEnabled:(BOOL)scrollEnabled{
    _segmentedPager.pager.scrollEnabled = scrollEnabled;
}

-(void)configureSegmentPager{
    self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedPager.segmentedControl.selectionIndicatorHeight = 3.0;
    self.segmentedPager.segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    self.segmentedPager.segmentedControl.backgroundColor = [UIColor whiteColor];
    
    self.activeStyleSheet = GS_StyleSheetActiveTab;
    self.deactiveStyleSheet = GS_StyleSheetDeactiveTab;

    self.segmentedPager.segmentedControl.selectionIndicatorColor = [UIColor colorWithRed:2/255.0 green:82/255.0 blue:150/255.0 alpha:1.0];
    self.segmentedPager.segmentedControl.selectedTitleTextAttributes =     @{
      NSForegroundColorAttributeName : [UIColor colorWithRed:2/255.0 green:82/255.0 blue:150/255.0 alpha:1.0]
    };
    self.segmentedPager.segmentedControl.segmentEdgeInset = UIEdgeInsetsMake(0, 10, 0, 10);
    
    [self addSubview:self.segmentedPager];
    
    if(_parallaxHeaderView){
        _segmentedPager.parallaxHeader.view = _parallaxHeaderView;
        _segmentedPager.parallaxHeader.mode = MXParallaxHeaderModeFill;
        _segmentedPager.parallaxHeader.height = 280;
        _segmentedPager.parallaxHeader.minimumHeight = 64+44+16;
        
        _segmentedPager.segmentedControl.backgroundColor = [UIColor clearColor];
        _segmentedPager.segmentedControl.titleTextAttributes =
        @{
          NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:0.69],
          NSFontAttributeName : [GSStyleSheet fontWithStyleSheet:self.deactiveStyleSheet]
          };
        
        _segmentedPager.segmentedControl.selectedTitleTextAttributes =
        @{
          NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:1.0],
          NSFontAttributeName : [GSStyleSheet fontWithStyleSheet:self.activeStyleSheet]
          };
        [_segmentedPager.segmentedControl setNeedsDisplay];
    }
    
    
    // for droping shadow of segment bar

    dispatch_async(dispatch_get_main_queue(), ^{
        [_segmentedPager.segmentedControl.superview bringSubviewToFront:_segmentedPager.segmentedControl];
    });
    _segmentedPager.segmentedControl.layer.shadowColor = [[UIColor blackColor] CGColor] ;
    _segmentedPager.segmentedControl.layer.shadowOpacity = 0.3;
    _segmentedPager.segmentedControl.layer.shadowRadius = 2.0;
    _segmentedPager.segmentedControl.layer.shadowOffset = CGSizeMake(0, 2.0);
    [_segmentedPager.segmentedControl setNeedsDisplay];
}

-(void)reloadData{
    NSInteger currentIndex = [self currentSegmentIndex];
    NSInteger totalPages = [self numberOfPagesInSegmentedPager:self.segmentedPager];
    if(currentIndex != 0 && currentIndex >= totalPages){
        [self.segmentedPager.pager showPageAtIndex:0 animated:NO];
    }
    [self.segmentedPager reloadData];
}

-(void)setParallaxHeaderView:(UIView *)view{
    _parallaxHeaderView = view;
    [_segmentedPager removeFromSuperview];
    _segmentedPager = nil;
    [self configureSegmentPager];
}

-(void)selectSegmentAtIndex:(NSInteger )index withAnimation:(BOOL)animation{
    [self.segmentedPager.pager showPageAtIndex:index animated:animation];
}


#pragma -MARK MXSegmentedPager DataSource

- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager{
    NSInteger noOfSegments = 0;
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(noOfSegmentInSegmentPager:)]){
        noOfSegments = [self.dataSource noOfSegmentInSegmentPager:self];
    }
    if(noOfSegments == 0){
        isTabZero = YES;
        return 1;
    }
    else {
        isTabZero = NO;
        return noOfSegments;
    }
}

-(NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index{
    NSString *title = @"";
    if(isTabZero == NO && self.dataSource && [self.dataSource respondsToSelector:@selector(segmentPager:titleForSegmentAtIndex:)]){
        title = [self.dataSource segmentPager:self titleForSegmentAtIndex:index];
    }
    return title;
}

- (UIView *)segmentedPager:(MXSegmentedPager *)segmentedPager viewForPageAtIndex:(NSInteger)index{
    UIViewController *viewController;
    if(isTabZero == NO && self.dataSource && [self.dataSource respondsToSelector:@selector(segmentPager:viewControllerForSegmentAtIndex:)]){
        viewController = [self.dataSource segmentPager:self viewControllerForSegmentAtIndex:index];
    }
    
    if(viewController != nil){
        return viewController.view;
    }
    else{
        return [UIView new];
    }
}

#pragma -MARK MXSegmentedPager Delegate

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectViewWithIndex:(NSInteger)index{
    if(lastSelectedSegmentIndex == index){
        return;
    }
    if(self.delegate && [self.delegate respondsToSelector:@selector(segmentPager:didMoveFromIndex:toIndex:)]){
        [self.delegate segmentPager:self didMoveFromIndex:lastSelectedSegmentIndex toIndex:index];
        lastSelectedSegmentIndex = index;
    }
}

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didScrollWithParallaxHeader:(MXParallaxHeader *)parallaxHeader{
    
    CGFloat maxHeight = _segmentedPager.parallaxHeader.height;
    CGFloat currentHeight = _segmentedPager.parallaxHeader.view.frame.size.height;
    CGFloat  minHeight = _segmentedPager.parallaxHeader.minimumHeight;
    
    if(_parallaxHeaderView == nil){
        return;
    }
    
    CGFloat progress = (currentHeight - minHeight) / (maxHeight - minHeight);
    //NSLog(@"::Progress::::::%f",progress);
    if(progress < 0.33){
        if(progress < 0.25){
            _segmentedPager.segmentedControl.backgroundColor = [UIColor whiteColor];
        }
        else{
            CGFloat diff = 0.33 - progress;
            diff *= 10;
            _segmentedPager.segmentedControl.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:diff];
        }
        _segmentedPager.segmentedControl.titleTextAttributes =
        @{
          NSForegroundColorAttributeName : [[UIColor blackColor] colorWithAlphaComponent:0.69],
          NSFontAttributeName : [GSStyleSheet fontWithStyleSheet:self.deactiveStyleSheet]
          };
        
        _segmentedPager.segmentedControl.selectedTitleTextAttributes =
        @{
          NSForegroundColorAttributeName : [[UIColor blackColor] colorWithAlphaComponent:1.0],
          NSFontAttributeName : [GSStyleSheet fontWithStyleSheet:self.activeStyleSheet]
          };
        [_segmentedPager.segmentedControl setNeedsDisplay];
    }
    else{
        _segmentedPager.segmentedControl.backgroundColor = [UIColor clearColor];
        _segmentedPager.segmentedControl.titleTextAttributes =
        @{
          NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:0.69],
          NSFontAttributeName : [GSStyleSheet fontWithStyleSheet:self.deactiveStyleSheet]
          };
        
        _segmentedPager.segmentedControl.selectedTitleTextAttributes =
        @{
          NSForegroundColorAttributeName : [[UIColor whiteColor] colorWithAlphaComponent:1.0],
          NSFontAttributeName : [GSStyleSheet fontWithStyleSheet:self.activeStyleSheet]
          };
        [_segmentedPager.segmentedControl setNeedsDisplay];
    }
    if(self.delegate && [self.delegate respondsToSelector:@selector(segmentPager:parallaxHeaderDidScrollWithProgress:)]){
        [self.delegate segmentPager:self parallaxHeaderDidScrollWithProgress:progress];
    }
    
}


@end
