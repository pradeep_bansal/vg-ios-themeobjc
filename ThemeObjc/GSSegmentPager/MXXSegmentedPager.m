//
//  MXXSegmentedPager.m
//  DemoParallaxSegmentedPager
//
//  Created by Vasim Akram on 2/10/17.
//  Copyright © 2017 GirnarSoft. All rights reserved.
//

#import "MXXSegmentedPager.h"

#define kSegmentHeight 44.0
#define kOffsetMargin 16.0

@implementation MXXSegmentedPager

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
 */



- (void)layoutSegmentedControl {
    CGRect frame = self.bounds;
    
    frame.origin.x = self.segmentedControlEdgeInsets.left;
    
    if (self.segmentedControlPosition == MXSegmentedControlPositionTop) {
        frame.origin.y = self.segmentedControlEdgeInsets.top;
    } else {
        frame.origin.y  = frame.size.height;
        frame.origin.y -= kSegmentHeight;
        frame.origin.y -= self.segmentedControlEdgeInsets.bottom;
    }
    
    frame.size.width -= self.segmentedControlEdgeInsets.left;
    frame.size.width -= self.segmentedControlEdgeInsets.right;
    frame.size.height = kSegmentHeight;
    
    frame.origin.y -= (kSegmentHeight + kOffsetMargin);
    
    self.segmentedControl.frame = frame;
}

- (void)layoutPager {
    CGRect frame = self.bounds;
    
    frame.origin = CGPointZero;
    
    if (self.segmentedControlPosition == MXSegmentedControlPositionTop) {
        frame.origin.y  = kSegmentHeight;
        frame.origin.y += self.segmentedControlEdgeInsets.top;
        frame.origin.y += self.segmentedControlEdgeInsets.bottom;
    }
    frame.size.height -= kSegmentHeight;
    frame.size.height -= self.segmentedControlEdgeInsets.top;
    frame.size.height -= self.segmentedControlEdgeInsets.bottom;
    frame.size.height -= self.parallaxHeader.minimumHeight;
    frame.origin.y -= (kSegmentHeight + kOffsetMargin);
    frame.size.height += (kSegmentHeight + kOffsetMargin);
    self.pager.frame = frame;
}

@end
