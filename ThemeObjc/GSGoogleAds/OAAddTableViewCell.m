//
//  OAAddTableViewCell.m
//  DemoGoogleAdd
//
//  Created by Vasim Akram on 7/11/17.
//  Copyright © 2017 GirnarSoft. All rights reserved.
//

#import "OAAddTableViewCell.h"
#import "HCData.h"

@interface OAAddTableViewCell()<GADBannerViewDelegate, GADAdSizeDelegate>

@end

@implementation OAAddTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblLoading.hidden = NO;
    self.bannerView.delegate = self;
    self.bannerView.adSizeDelegate = self;
    
    self.lblAd.styleSheet = GS_StyleSheetCaption1;
    self.lblLoading.styleSheet = GS_StyleSheetBody1Black;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)adViewDidReceiveAd:(DFPBannerView *)adView {
    self.lblLoading.hidden = YES;
}

- (void)adView:(GADBannerView *)bannerView willChangeAdSizeTo:(GADAdSize)size{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error{
    NSLog(@"adView didFailToReceiveAdWithError: %@",error.localizedDescription);
    if(bannerView.rootViewController && [bannerView.rootViewController respondsToSelector:@selector(reloadTableViewByRemovingAd:)] == YES){
        [bannerView.rootViewController performSelector:@selector(reloadTableViewByRemovingAd:) withObject:self.addData];
    }
    else{
        NSLog(@"reloadTableViewByRemovingAd: is missing");
    }
}

-(void)reloadTableViewByRemovingAd:(id)addData{
    //to remove warning
}

@end
