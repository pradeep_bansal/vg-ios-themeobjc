//
//  HCHomeConfigModel.m
//
//  Created by Sourabh Goyal on 4/27/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "HCHomeConfigModel.h"
#import "HCAds.h"


NSString *const kHCHomeConfigModelAds = @"ads";


@interface HCHomeConfigModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HCHomeConfigModel

@synthesize ads = _ads;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedHCAds = [dict objectForKey:kHCHomeConfigModelAds];
    NSMutableArray *parsedHCAds = [NSMutableArray array];
    if ([receivedHCAds isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedHCAds) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedHCAds addObject:[HCAds modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedHCAds isKindOfClass:[NSDictionary class]]) {
       [parsedHCAds addObject:[HCAds modelObjectWithDictionary:(NSDictionary *)receivedHCAds]];
    }

    self.ads = [NSArray arrayWithArray:parsedHCAds];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForAds = [NSMutableArray array];
    for (NSObject *subArrayObject in self.ads) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForAds addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForAds addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForAds] forKey:kHCHomeConfigModelAds];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.ads = [aDecoder decodeObjectForKey:kHCHomeConfigModelAds];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_ads forKey:kHCHomeConfigModelAds];
}

- (id)copyWithZone:(NSZone *)zone
{
    HCHomeConfigModel *copy = [[HCHomeConfigModel alloc] init];
    
    if (copy) {

        copy.ads = [self.ads copyWithZone:zone];
    }
    
    return copy;
}


@end
