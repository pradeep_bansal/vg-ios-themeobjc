//
//  HCAds.m
//
//  Created by Sourabh Goyal on 4/27/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "HCAds.h"
#import "HCData.h"


NSString *const kHCAdsScreenName = @"screenName";
NSString *const kHCAdsData = @"data";


@interface HCAds ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HCAds

@synthesize screenName = _screenName;
@synthesize data = _data;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.screenName = [self objectOrNilForKey:kHCAdsScreenName fromDictionary:dict];
    NSObject *receivedHCData = [dict objectForKey:kHCAdsData];
    NSMutableArray *parsedHCData = [NSMutableArray array];
    if ([receivedHCData isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedHCData) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedHCData addObject:[HCData modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedHCData isKindOfClass:[NSDictionary class]]) {
       [parsedHCData addObject:[HCData modelObjectWithDictionary:(NSDictionary *)receivedHCData]];
    }

    self.data = [NSArray arrayWithArray:parsedHCData];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.screenName forKey:kHCAdsScreenName];
    NSMutableArray *tempArrayForData = [NSMutableArray array];
    for (NSObject *subArrayObject in self.data) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForData addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForData addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForData] forKey:kHCAdsData];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.screenName = [aDecoder decodeObjectForKey:kHCAdsScreenName];
    self.data = [aDecoder decodeObjectForKey:kHCAdsData];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_screenName forKey:kHCAdsScreenName];
    [aCoder encodeObject:_data forKey:kHCAdsData];
}

- (id)copyWithZone:(NSZone *)zone
{
    HCAds *copy = [[HCAds alloc] init];
    
    if (copy) {

        copy.screenName = [self.screenName copyWithZone:zone];
        copy.data = [self.data copyWithZone:zone];
    }
    
    return copy;
}


@end
