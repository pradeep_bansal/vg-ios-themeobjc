//
//  GSGoogleMobileAds.h
//  ThemeObjc
//
//  Created by Jogendra Singh on 28/04/17.
//  Copyright © 2017 GirnarSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "HCData.h"

#define kAdCity @"city"
#define kAdAppVersion @"appVersion"
#define kAdBrand @"Brand"
#define kAdModel @"Model"
#define kAdBodyType @"bodyType"
#define kAdPrice @"Price"
#define kAdScreenName @"screenName"
#define kAdSiteName @"SiteName"


@protocol GSGoogleMobileAdDelegate <NSObject>

@optional
-(void)reloadTableViewByRemovingAd:(id)addData;

@end


@interface GSGoogleMobileAds : NSObject

+ (id)sharedInstance;

+(void)setGoogleAdsUnitId:(NSString *)adUnitIdStr;
+(void) addGoogleMobileAdsInViewModelArray:(NSMutableArray *)viewModelArray withScreenName:(NSString*)screenName;
+(UITableViewCell *) getGoogleMobileAdsCellWithData:(HCData *) adData andInfo:(NSDictionary *)dictInfo forIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView andTarget:(UIViewController *)controller;
+(void)registerCellToReuseForTableView:(UITableView *)tableView;
+(void)updateGoogleAddData:(NSDictionary *)data;


@end
