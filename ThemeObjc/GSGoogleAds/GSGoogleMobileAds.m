//
//  GSGoogleMobileAds.m
//  ThemeObjc
//
//  Created by Jogendra Singh on 28/04/17.
//  Copyright © 2017 GirnarSoft. All rights reserved.
//

#import "GSGoogleMobileAds.h"
#import "OAAddTableViewCell.h"
#import "HCAds.h"
#import "HCHomeConfigModel.h"

static NSString *cellIdentifer = @"OAAddTableViewCell";


@interface GSGoogleMobileAds ()

@property (nonatomic, strong) NSString *googleAdUnitId;

@end

@implementation GSGoogleMobileAds


static GSGoogleMobileAds *sharedAds = nil;
+ (id)sharedInstance
{
    @synchronized([GSGoogleMobileAds class])
    {
        if(sharedAds == nil){
            sharedAds = [[self alloc] init];
        }
    }
    return sharedAds;
}

+(NSString *)getGoogleAdUnitId{
    GSGoogleMobileAds *objAds = [GSGoogleMobileAds sharedInstance];
    if(objAds.googleAdUnitId == nil){
        NSLog(@":::::::::googleAdUnitId::::::is missing:::::::::");
        return @"";
    }
    return objAds.googleAdUnitId;
}

+(void)setGoogleAdsUnitId:(NSString *)adUnitIdStr{
    GSGoogleMobileAds *objAds = [GSGoogleMobileAds sharedInstance];
    objAds.googleAdUnitId = adUnitIdStr;
}
+(void) addGoogleMobileAdsInViewModelArray:(NSMutableArray *)viewModelArray withScreenName:(NSString*)screenName{
    
    HCAds *adMobData = [GSGoogleMobileAds googleAdsDataForScreen:screenName];
    if (viewModelArray.count == 0 || adMobData.data.count == 0){
        return;
    }
    
    //Removing old Data
    NSMutableArray *adsToRemove = [NSMutableArray array];
    for (id item in viewModelArray) {
        if([item isKindOfClass:[HCData class]]){
            [adsToRemove addObject:item];
        }
    }
    [viewModelArray removeObjectsInArray:adsToRemove];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pos" ascending:YES];
    NSArray *sortedArray = [adMobData.data sortedArrayUsingDescriptors:@[sortDescriptor]];
    for (HCData *adData in sortedArray) {
        NSInteger adIndex = adData.pos;
        if( (viewModelArray.count) > adIndex ) {
            [viewModelArray insertObject:adData atIndex:adIndex];
        }else{
            [viewModelArray addObject:adData];
            break;
        }
    }
}
+(HCAds *)googleAdsDataForScreen :(NSString *)screenName {
    HCAds *finalScreenAds = [[HCAds alloc]init];
    NSDictionary *adsDataDic = [NSDictionary dictionaryWithContentsOfFile:[GSGoogleMobileAds getGoogleAddFilePath]];
    if (adsDataDic != nil) {
        HCHomeConfigModel *model = [HCHomeConfigModel modelObjectWithDictionary:adsDataDic];
        NSArray *adsArray = model.ads;
        NSPredicate * resultPredicate = [NSPredicate predicateWithFormat:@"screenName == %@", screenName];
        NSArray *filtredArray  = [adsArray filteredArrayUsingPredicate:resultPredicate];
        
        if (filtredArray.count > 0 ){
            finalScreenAds = filtredArray[0];
            return finalScreenAds;
        }
    }
    return finalScreenAds;
}

+(NSString *)getGoogleAddFilePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"AdMobData.plist"];
    return filePath;
}

+(void)registerCellToReuseForTableView:(UITableView *)tableView{
    //Registerd TableView Cell with TableView
    NSBundle *bundle = [NSBundle bundleForClass:[OAAddTableViewCell class]];
    [tableView registerNib:[UINib nibWithNibName:cellIdentifer bundle:bundle] forCellReuseIdentifier:cellIdentifer];
}


+(UITableViewCell *) getGoogleMobileAdsCellWithData:(HCData *) adData andInfo:(NSDictionary *)dictInfo forIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView andTarget:(UIViewController *)controller
{
    
    OAAddTableViewCell *cell = (OAAddTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifer forIndexPath:indexPath];
    
    NSString *adUnitIdStr = [NSString stringWithFormat:@"/%@/%@",[GSGoogleMobileAds getGoogleAdUnitId],adData.key];
    if([cell.bannerView.adUnitID isEqualToString:adUnitIdStr] == NO){

        cell.bannerView.adSize = [self getAdSizeFor:adData.type];
        
        cell.bannerView.adUnitID = adUnitIdStr;
        cell.bannerView.rootViewController = controller;
        DFPRequest *request = [DFPRequest request];
        
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc]initWithDictionary:dictInfo];
        newDict= [self getAdsNewDict:dictInfo];
        if (newDict != nil){
        request.customTargeting = newDict;
        }
        [cell.bannerView loadRequest:request];
        //[cell.bannerView performSelectorInBackground:@selector(loadRequest:) withObject:request];
    }
    cell.tableView = tableView;
    cell.addData = adData;
    
    return cell;
}

+(void)updateGoogleAddData:(NSDictionary *)data{
    BOOL success = [data writeToFile:[GSGoogleMobileAds getGoogleAddFilePath] atomically:YES];
    if(success == YES){
        NSLog(@"Google Add File Updated Successfully");
    }
    else{
        NSLog(@"Error !!! in updating Google Add File");
    }
}

+(NSMutableDictionary* )getAdsNewDict:(NSDictionary *)dictInfo{
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc]init];
    
    NSString *city = [[UIApplication sharedApplication].delegate performSelector:@selector(getUserCity) withObject:nil];
    NSString *appVersion = [NSString stringWithFormat:@"%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    NSString *siteName = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] bundleIdentifier]];
    
    newDict[kAdCity] = city;
    newDict[kAdAppVersion] = appVersion;
    newDict[kAdSiteName] = siteName;
    
    if ([[dictInfo allKeys] containsObject:kAdBodyType]){
        newDict[kAdBodyType] = dictInfo[kAdBodyType];
    }
    if ([[dictInfo allKeys] containsObject:kAdScreenName]){
        newDict[kAdScreenName] = dictInfo[kAdScreenName];
    }
    
    if ([[dictInfo allKeys] containsObject:kAdPrice]){
        NSString *price = [GSGoogleMobileAds getRangeForPriceSegment:[dictInfo valueForKey:kAdPrice]];
        newDict[kAdPrice] = price;
    }
    
    if ([[dictInfo allKeys] containsObject:kAdBrand]){
        if([dictInfo[kAdBrand] rangeOfString:@","].length !=0) {
            NSString *newString = dictInfo[kAdBrand];
            if([[newString substringFromIndex:[newString length] - 1] isEqualToString: @","]) {
                newString = [newString substringToIndex:[newString length]-1];
            }
            NSArray *arr = [newString componentsSeparatedByString:@","];
            newDict[kAdBrand] = arr;
        }
        else {
            newDict[kAdBrand] = dictInfo[kAdBrand];
            if ([[dictInfo allKeys] containsObject:kAdModel]){
                if([dictInfo[kAdModel] rangeOfString: dictInfo[kAdBrand]].length ==0) {
                    newDict[kAdModel] = [NSString stringWithFormat: @"%@_%@", dictInfo[kAdBrand], dictInfo[kAdModel]];;
                }
            }
        }
    }
    return newDict;
}



+(GADAdSize )getAdSizeFor:(NSString *)type{
    if ([type  isEqualToString:@"FLUID"]) {
        return kGADAdSizeFluid;
    }
    else if ([type  isEqualToString:@"MEDIUM_RECTANGLE"])  {
        return kGADAdSizeMediumRectangle;
    }
    else if ([type  isEqualToString:@"BANNER"])  {
        return kGADAdSizeBanner;
    }
    else if ([type  isEqualToString:@"FULL_BANNER"])  {
        return kGADAdSizeFullBanner;
    }
    else if ([type  isEqualToString:@"LARGE_BANNER"])  {
        return kGADAdSizeLargeBanner;
    }
    else if ([type  isEqualToString:@"LEADERBOARD"])  {
        return kGADAdSizeLeaderboard;
    }
    else if ([type  isEqualToString:@"SMART_BANNER"])  {
        return kGADAdSizeSmartBannerPortrait;
    }
    else if ([type  isEqualToString:@"WIDE_SKYSCRAPER"])  {
        return kGADAdSizeSkyscraper;
    }
    else if ([self isCustomSize:type])  {
        NSArray *arrSizeData = [type componentsSeparatedByString:@"x"];
        CGSize size = CGSizeMake([arrSizeData.firstObject doubleValue], [arrSizeData.lastObject doubleValue]);
        return GADAdSizeFromCGSize(size);
    }
    else{
        return kGADAdSizeBanner;
    }
}

+(BOOL)isCustomSize:(NSString *)type{
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^\\d+x\\d+$"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    if(error){
        return NO;
    }
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:type
                                                        options:0
                                                          range:NSMakeRange(0, [type length])];
    if(numberOfMatches > 0){
        return YES;
    }
    return NO;
}

+(NSString*)getRangeForPriceSegment:(NSString *)comparePrice {
    
    long price = comparePrice.integerValue;
    if(price <= 300000)
    {
        return @"upto3L";
    }
    else if(price <= 600000)
    {
        return @"3-6L";
    }
    else if(price <= 1200000)
    {
        return @"6-12L";
    }
    else if(price <= 1500000)
    {
        return @"12-15L";
    }
    else if(price <= 2000000)
    {
        return @"15-20L";
    }
    else
    {
        return @"20Labove";
    }
}

@end
