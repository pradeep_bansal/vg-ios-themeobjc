//
//  HCData.m
//
//  Created by Sourabh Goyal on 4/27/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "HCData.h"


NSString *const kHCDataKey = @"key";
NSString *const kHCDataPos = @"pos";
NSString *const kHCDataType = @"type";


@interface HCData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation HCData

@synthesize key = _key;
@synthesize pos = _pos;
@synthesize type = _type;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.key = [self objectOrNilForKey:kHCDataKey fromDictionary:dict];
            self.pos = [[self objectOrNilForKey:kHCDataPos fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kHCDataType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.key forKey:kHCDataKey];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pos] forKey:kHCDataPos];
    [mutableDict setValue:self.type forKey:kHCDataType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.key = [aDecoder decodeObjectForKey:kHCDataKey];
    self.pos = [aDecoder decodeDoubleForKey:kHCDataPos];
    self.type = [aDecoder decodeObjectForKey:kHCDataType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_key forKey:kHCDataKey];
    [aCoder encodeDouble:_pos forKey:kHCDataPos];
    [aCoder encodeObject:_type forKey:kHCDataType];
}

- (id)copyWithZone:(NSZone *)zone
{
    HCData *copy = [[HCData alloc] init];
    
    if (copy) {

        copy.key = [self.key copyWithZone:zone];
        copy.pos = self.pos;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}

@end
