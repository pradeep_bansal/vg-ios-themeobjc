//
//  HCHomeConfigModel.h
//
//  Created by Sourabh Goyal on 4/27/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface HCHomeConfigModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSArray *ads;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
